import 'dart:async';

import 'package:bloc_app/BLoC/Bloc.dart';
import 'package:bloc_app/DataLayer/Location.dart';
import 'package:bloc_app/DataLayer/ZomotoClient.dart';

class LocationQueryBloc implements Bloc {
  final _controller = StreamController<List<Location>>();
  final _client = ZomotoClient();

  Stream<List<Location>> get locationStream => _controller.stream;

  void submitQuery(String query) async {
    final results = await _client.fetchLocations(query);
    _controller.sink.add(results);
  }

  @override
  void dispose() {
    _controller.close();
  }
}
