import 'dart:async';

import 'package:bloc_app/BLoC/Bloc.dart';
import 'package:bloc_app/DataLayer/Location.dart';
import 'package:bloc_app/DataLayer/Restaurant.dart';
import 'package:bloc_app/DataLayer/ZomotoClient.dart';

class RestaurantBloc implements Bloc {
  final Location location;
  final _controller = StreamController<List<Restaurant>>();
  final _zomotoClient = ZomotoClient();

  Stream<List<Restaurant>> get stream => _controller.stream;

  RestaurantBloc(this.location);

  void submitQuery(String query) async {
    final results = await _zomotoClient.fetchRestaurants(location, query);
    _controller.sink.add(results);
  }

  @override
  void dispose() {
    _controller.close();
  }
}
