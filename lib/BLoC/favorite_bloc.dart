import 'dart:async';

import 'package:bloc_app/BLoC/Bloc.dart';
import 'package:bloc_app/DataLayer/Restaurant.dart';

class FavoriteBloc implements Bloc {
  var _restaurants = <Restaurant>[];

  List<Restaurant> get favorites => _restaurants;
  final _controller = StreamController<List<Restaurant>>.broadcast();

  Stream<List<Restaurant>> get favoriteStream => _controller.stream;

  void toggleRestaurant(Restaurant restaurant) {
    if (_restaurants.contains(restaurant)) {
      _restaurants.remove(restaurant);
    } else {
      _restaurants.add(restaurant);
    }
    _controller.sink.add(_restaurants);
  }

  @override
  void dispose() {
    _controller.close();
  }
}
