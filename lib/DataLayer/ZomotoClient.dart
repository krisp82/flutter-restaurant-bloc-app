import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:bloc_app/DataLayer/Restaurant.dart';
import 'package:http/http.dart' as http;
import 'package:bloc_app/DataLayer/Location.dart';
import 'package:meta/meta.dart';

class ZomotoClient {
  final _apiKey = "4c23420ef000df112699a3d48ff99242";
  final _host = "developers.zomato.com";
  final _contextRoot = "api/v2.1";

  Future<List<Location>> fetchLocations(String query) async {
    final results = await request(
        path: 'locations', parameters: {'query': query, 'count': '10'});
    final suggestions = results['location_suggestions'];
    return suggestions
        .map<Location>((json) => Location.fromJson(json))
        .toList(growable: false);
  }

  Future<List<Restaurant>> fetchRestaurants(
      Location location, String query) async {
    final results = await request(path: 'search', parameters: {
      'entity_id': location.id.toString(),
      'entity_type': location.type,
      'q': query,
      'count': '10',
    });

    final restaurants = results['restaurants']
        .map<Restaurant>((json) => Restaurant.fromJson(json['restaurant']))
        .toList(growable: false);
    return restaurants;
  }

  Future<Map> request(
      {@required String path, Map<String, String> parameters}) async {
    HttpClient httpClient = new HttpClient();
    httpClient.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    final url = Uri.https(_host, '$_contextRoot/$path', parameters);
    HttpClientRequest clientRequest = await httpClient.getUrl(url);
    clientRequest.headers.set('Accept', 'applcation/json');
    clientRequest.headers.set('user-key', _apiKey);
    HttpClientResponse httpClientResponse = await clientRequest.close();
    final data = await httpClientResponse.transform(utf8.decoder).join();
    final jsonObject = json.decode(data);
    return jsonObject;
  }
}
