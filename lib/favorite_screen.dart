import 'package:bloc_app/BLoC/bloc_provider.dart';
import 'package:bloc_app/BLoC/favorite_bloc.dart';
import 'package:bloc_app/DataLayer/Restaurant.dart';
import 'package:bloc_app/restaurant_tile.dart';
import 'package:flutter/material.dart';

class FavoriteScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<FavoriteBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Favorites'),
      ),
      body: StreamBuilder(
          stream: bloc.favoriteStream,
          initialData: bloc.favorites,
          builder: (context, snapshot) {
            List<Restaurant> favorites =
                (snapshot.connectionState == ConnectionState.waiting)
                    ? bloc.favorites
                    : snapshot.data;
            if (favorites == null || favorites.isEmpty) {
              return Center(
                child: Text('No Favorites'),
              );
            }
            return ListView.separated(
                itemBuilder: (context, index) {
                  final restaurant = favorites[index];
                  return RestaurantTile(
                    restaurant: restaurant,
                  );
                },
                separatorBuilder: (context, index) => Divider(),
                itemCount: favorites.length);
          }),
    );
  }
}
