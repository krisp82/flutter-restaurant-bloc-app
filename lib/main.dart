import 'package:bloc_app/BLoC/LocationBloc.dart';
import 'package:bloc_app/BLoC/bloc_provider.dart';
import 'package:bloc_app/BLoC/favorite_bloc.dart';
import 'package:flutter/material.dart';
import 'package:bloc_app/main_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<LocationBloc>(
        bloc: LocationBloc(),
        child: BlocProvider<FavoriteBloc>(
          bloc: FavoriteBloc(),
          child: MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            home: MainScreen(),
          ),
        ));
  }
}
