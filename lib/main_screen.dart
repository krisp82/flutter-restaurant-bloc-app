import 'package:bloc_app/BLoC/LocationBloc.dart';
import 'package:bloc_app/BLoC/bloc_provider.dart';
import 'package:bloc_app/DataLayer/Location.dart';
import 'package:bloc_app/restaurant_screen.dart';
import 'package:flutter/material.dart';
import 'package:bloc_app/location_screen.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Location>(
      stream: BlocProvider.of<LocationBloc>(context).locationStream,
      builder: (context, snapshot) {
        final location = snapshot.data;
        if (location == null) {
          return LocationScreen();
        }
        return RestaurantScreen(location: location,);
      },
    );
  }
}
